# PlatonsPlatform

Unity Project on "Light vs. Darkness"

Dieses Project entsteht im Rahmen eines 2-monatigen Praktikums an der Ruhr Universität Bochum. Die Grundlagen des Programmierens, Animierens und Design in Unity wurden vermittelt und sollen in einem Abschlussprojekt wiedergegeben werden. 
Dies ist eines dieser Projekte. 

Jedes Projekt stellt ein Computerspiel dar, welches im Rahmen eines 'Game Jams' mit dem Thema 'Light vs. Darkness' entstanden ist.

Platons Platform ist spielbar in meinem "[Kaninchenbau](https://derkaninchenbau.de)"


## Idee des Spiels
Um das Thema "Light vs Darkness" zu erfüllen ist die Idee dieses Spiels, den Spieler nur den Schatten der eigentlichen Welt zu zeigen. Seine Aufgabe ist es das Licht zu erreichen.

Als Inspiration ist hier Platons Höhlengleichnis zu nennen. In diesem philosophischen Gedankenexperiment hält eine Person die Schatten die sie sieht für die eigentliche Realität. Für sie existiert in der Welt nicht mehr als diese Schatten. In dem Moment in dem diese Person die Höhle in der sie sitzt verlässt, bricht ihr gesamtes Weltbild zusammen.

Diese Erfahrung soll dem Spieler mit diesem Spiel vermittelt werden. Dass das was er sieht (die Schatten) nur ein Abbild der eigentlichen Welt sind.


![MainMenu.png](PreviewPictures/PlatonsPlatform1.png)

![FirstLevel.png](PreviewPictures/PlatonsPlatform2.png)
