﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pendelLimits : MonoBehaviour
{
    HingeJoint hinge;


    void Awake()
    {
        hinge = GetComponent<HingeJoint>();
    }


    void Update()
    {
        checkHingeRot();
    }


    public void checkHingeRot()
    {

        var motor = hinge.motor;
        Debug.Log(hinge.gameObject.transform.rotation.x);
        if(hinge.gameObject.transform.rotation.x>=.11f)
        {
            motor.targetVelocity = -10f;
        }
        if(hinge.gameObject.transform.rotation.x<=-.11f)
        {
            motor.targetVelocity = 10f;
        }

        hinge.motor = motor;

    }
}
