﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
   
    GameObject spieler;
    [SerializeField]float leftDistance = 10f;
    [SerializeField]float rightDistance = 2f;

    void Awake()
    {
        spieler = GameObject.Find("Spieler");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        moveCamera();
    }

    void moveCamera()
    {
        if(transform.position.x + rightDistance < spieler.transform.position.x)
        {
            transform.position = new Vector3(spieler.transform.position.x - rightDistance,transform.position.y,transform.position.z);
        }
        else if (spieler.transform.position.x + leftDistance < transform.position.x)
        {
            transform.position = new Vector3(spieler.transform.position.x + leftDistance,transform.position.y,transform.position.z);
        }
    }
}
