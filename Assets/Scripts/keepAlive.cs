﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keepAlive : MonoBehaviour
{

    AudioSource player;
    
    void Start ()
    {
        DontDestroyOnLoad(this.gameObject);
        player = gameObject.GetComponent<AudioSource>();
    }

    public void playSound(AudioClip clip)
    {
        player.PlayOneShot(clip);
    }
}
