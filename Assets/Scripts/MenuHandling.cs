﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MenuHandling : MonoBehaviour
{

    GameObject mainMenu;
    GameObject statMenu;

    int lvlToResume = 0;






    void Awake()
    {
        GameObject mainMenu = GameObject.Find("MainMenu");
        GameObject statMenu = GameObject.Find("StatsMenu");
    }


    void Start()
    {
        Text lvlDisplay = GameObject.Find("LvlDisplay").GetComponent<Text>();
        lvlToResume = PlayerPrefs.GetInt("lvlsFinished") + 1;
        lvlDisplay.text = "<b>Level "+ lvlToResume+"</b>";
        Text lvlDisplayShadow = GameObject.Find("LvlDisplayShadow").GetComponent<Text>();
        lvlDisplayShadow.text = "<b>Level "+ lvlToResume+"</b>";
        if(lvlToResume==1)
        {
            GameObject resumeButton = GameObject.Find("ResumeButton");
            TMP_Text resumeTextGrey = GameObject.Find("Resume").GetComponent<TMP_Text>();
            resumeButton.GetComponent<Button>().interactable = false;
            resumeTextGrey.color = new Color(0f,0f,0f,0f);
            lvlDisplay.color = new Color(0f,0f,0f,0f);
            lvlDisplayShadow.color = new Color(0f,0f,0f,0f);

        }



        // Grey Out and disable the Stats Button since it is not implemented!
        GameObject statButton = GameObject.Find("StatsButton");
        TMP_Text statTextGrey = GameObject.Find("Stats").GetComponent<TMP_Text>();
        if(true)
        {
            statButton.GetComponent<Button>().interactable = false;
            statTextGrey.color = new Color(0f,0f,0f,0f);
        }
    }



    public void startNew()
    {
        SceneManager.LoadScene(1);
    }

    public void resume()
    {
        if(lvlToResume!=0)
        {
            SceneManager.LoadScene(lvlToResume);
        }
    }

    public void showStats()
    {

    }

    public void quit()
    {
        Application.Quit();
    }

    public void backToMainMenu()
    {
        
    }

}
