﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainMenuPlayerAnimation : MonoBehaviour
{
    
    Rigidbody rb;
    RaycastHit hit;

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }
    void Start()
    {
        StartCoroutine(randomMovement());
    }
    

    IEnumerator randomMovement()
    {
        Vector3 direction = new Vector3(Random.Range(-1f,1f)*2f,Random.Range(0.6f,1f)*4f,0f);
        if(Physics.Raycast(transform.position,Vector3.down,0.5f))
        {
           rb.AddForce(direction*1000); 
        }
        yield return new WaitForSeconds(1f);
        StartCoroutine(randomMovement());
    }

}
