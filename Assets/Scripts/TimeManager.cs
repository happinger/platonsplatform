﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    [SerializeField]Text overallTimeText;
    [SerializeField]Text bestTimeText;
    [SerializeField]Text currentTimeText;
    

    float overallTime;
    float bestTime;
    float currentTime;
    int lvlIndex;
    bool levelWasBeatenOnce;
    bool levelWasBeatenNow;

    void Awake()
    {
        lvlIndex = SceneManager.GetActiveScene().buildIndex;
        overallTime = PlayerPrefs.GetFloat("overallTimeLevel" + lvlIndex);
        bestTime = PlayerPrefs.GetFloat("bestTimeLevel"+lvlIndex);
        levelWasBeatenOnce = PlayerPrefs.GetInt("levelWasBeatenOnceLevel"+ lvlIndex)==1;
        if(levelWasBeatenOnce)
        {
            overallTimeText.text = "Time to beat this Level: "+overallTime.ToString("F2")+ "s";
            bestTimeText.text = "Best time for this Level: "+bestTime.ToString("F2")+"s";
        }

        currentTime = 0f;
        levelWasBeatenNow = false;
    }



    void Update()
    {
        updateTime();
    }


    void updateTime()
    {
        if(!levelWasBeatenNow)
        {
            currentTime += Time.deltaTime;
            currentTimeText.text = "Current time: " + currentTime.ToString("F2") + "s";
        }


        if(!levelWasBeatenOnce)
        {
            overallTime += Time.deltaTime;
            overallTimeText.text = "Time to beat this Level: "+overallTime.ToString("F2")+ "s";
            PlayerPrefs.SetFloat("overallTimeLevel"+lvlIndex,overallTime);
        }

    }

    public void finishedLevelTimeCheck()
    {
        levelWasBeatenNow = true;
        
        if(currentTime < bestTime || bestTime == 0)
        {
            bestTime = currentTime;
            PlayerPrefs.SetFloat("bestTimeLevel"+lvlIndex, currentTime); 
            bestTimeText.text = "Best time for this Level: "+bestTime.ToString("F2")+"s";
        }

        if(!levelWasBeatenOnce)
        {
            levelWasBeatenOnce = true;
            PlayerPrefs.SetInt("levelWasBeatenOnceLevel"+lvlIndex,1);
        }
    }
}
