﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endScreenReturnButton : MonoBehaviour
{

    public lvlManager levelManager;

    void Update()
    {
        if(Input.GetButtonDown("Cancel"))
        {
            levelManager.loadMainMenu();
        }
    }

}
