﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TimeManager;
using static keepAlive;
using UnityEngine.UI;


public class PlayerBallMovement : MonoBehaviour
{



    [SerializeField]float inputThrust = 10f;
    [SerializeField]float jumpForce = 100f;
    [SerializeField]AudioClip jumpSound = default;
    float actualThrust;
    float horizontalMovement;
    float verticalMovement;
    bool pressedR;
    bool pressedN;
    bool pressedESC;
    bool pressedJump;
    Rigidbody rigidbody;
    RaycastHit hit;
    public lvlManager levelManager;
    keepAlive audioPlayer;
    Animator cameraAnimator;
    TimeManager timeManager;
    GameObject nextLevelUIText;






    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        timeManager = GameObject.Find("UI").GetComponent<TimeManager>();
        cameraAnimator = GameObject.Find("CameraHolder").GetComponent<Animator>();
        audioPlayer = GameObject.Find("Music").GetComponent<keepAlive>();
        nextLevelUIText = GameObject.Find("NextLevel UI Text");
        displayNextLevelInfo();     
    }





    void Update()
    {
        takeInput();
        applyInput();
    }




    void takeInput()
    {
        horizontalMovement = Input.GetAxis("Horizontal");
        verticalMovement = Input.GetAxis("Vertical");
        pressedR = Input.GetKeyDown(KeyCode.R);
        pressedN = Input.GetKeyDown(KeyCode.N);
        pressedESC = Input.GetButtonDown("Cancel");
        pressedJump = Input.GetButtonDown("Jump");


    }

    void applyInput()
    {

        //Double movement speed if Left Shift is Pressed
        if(Input.GetKey(KeyCode.LeftShift))
        {
            actualThrust = 3* inputThrust;
        }
        else
        {
            actualThrust = inputThrust;
        }

        //Bewegung Links,Rechts,Vorne,Hinten 
        rigidbody.AddForce(new Vector3(horizontalMovement,0f,verticalMovement)*actualThrust);

        //Prüfe ob der SPieler auf einer Plattform ist und falls ja, lasse ihn springen
        if(pressedJump && Physics.Raycast(transform.position,Vector3.down,0.7f))
        {
            rigidbody.AddForce(new Vector3(0f,100f,0f)*jumpForce);
            audioPlayer.playSound(jumpSound);
        }        

        //Scene neu starten
        if(pressedR && levelManager.getBuildIndex()!= 0)
        {
            levelManager.restartLevel();
        }

        //Nächstes Level laden, falls aktuelles bereits einmal gelöst wurde
        if(pressedN&& levelManager.getBuildIndex() != 0)
        {
            if(PlayerPrefs.GetInt("lvlsFinished")>= levelManager.getBuildIndex())
            {
                levelManager.nextLevel();
            }
        }

        //Zurück ins Hauptmenü
        if(pressedESC && levelManager.getBuildIndex() != 0)
        {
            levelManager.loadMainMenu();
        }

    }





    //Handling all Triggers in the Map
    //Including Level Restart Trigger and Level Finish Trigger
    void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("_restart"))
        {
            levelManager.restartLevel();
        }
        if(collider.CompareTag("_finish"))
        {
            //Updating Highscores and Time Counters
            timeManager.finishedLevelTimeCheck();
            //Starting Camera Animation
            cameraAnimator.SetBool("lvlFinished",true);
            //"lvlsFinished" keeps track of the highest Level that the Player completed. For Main Menu "Resume Button",etc...
            if(PlayerPrefs.GetInt("lvlsFinished")<levelManager.getBuildIndex())
            {
                PlayerPrefs.SetInt("lvlsFinished",levelManager.getBuildIndex());
            }
            nextLevelUIText.SetActive(true);
            
        }
    }
    


    void displayNextLevelInfo()
    {
        //deactivating NextLevel UI Text if lvl hasn been finished before
        if(!(PlayerPrefs.GetInt("lvlsFinished")>=levelManager.getBuildIndex()))
        {
            nextLevelUIText.SetActive(false);
        }
    }
}
