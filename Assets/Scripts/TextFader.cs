﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextFader : MonoBehaviour
{
    [SerializeField]TMP_Text textfeldPro = default;   
    public bool isImmediate = false;
    


    void Update()
    {
        if(isImmediate)
        {   

            StartCoroutine(TextFade());
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(!isImmediate)
        {
            StartCoroutine(TextFade());
        }
        
    }

    IEnumerator TextFade()
    {
        if(isImmediate)
        {
            yield return new WaitForSeconds(.5f);
        }
        float colorFadeState = textfeldPro.color.a;
        if(textfeldPro.color.a<255)
        {
            textfeldPro.color = new Color(0f,0f,0f,colorFadeState+0.5f*Time.deltaTime);
        }
        else
        {
            textfeldPro.color = new Color(0f,0f,0f,1f);
            
        }
        yield return null;
        StartCoroutine(TextFade());
        
    }

}
