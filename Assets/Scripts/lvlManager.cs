﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[CreateAssetMenu(fileName = "LVLS", menuName = "ScriptableObjects/lvlManager", order = 1)]
public class lvlManager : ScriptableObject
{

    Scene currentScene;
    int currentSceneIndex;
    bool inMainMenu = false;

    /*void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }*/



    public int getBuildIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public void restartLevel()
    {
        if(!inMainMenu)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

    public void nextLevel()
    {
        if (!inMainMenu)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1 );
        }
    }

    public void loadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

}
